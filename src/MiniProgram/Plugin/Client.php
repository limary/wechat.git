<?php
namespace Sinta\Wechat\MiniProgram\Plugin;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;

class Client extends BaseClient
{
    /**
     * 向插件开发者发起使用插件的申请
     *
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/applyPlugin.html
     *
     * @param $appId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function apply($appId, string $reason = null)
    {
        return $this->httpPostJson('wxa/plugin', [
            'action' => 'apply',
            'plugin_appid' => $appId,
            'reason' => $reason,
        ]);
    }

    /**
     * 查询已添加的插件
     * @https://developers.weixin.qq.com/miniprogram/dev/api-backend/getPluginList.html
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {
        return $this->httpPostJson('wxa/plugin', [
            'action' => 'list',
        ]);
    }

    /**
     * 删除已添加的插件
     *
     * @see https://developers.weixin.qq.com/miniprogram/dev/api-backend/unbindPlugin.html
     *
     * @param $appId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function unbind($appId)
    {
        return $this->httpPostJson('wxa/plugin', [
            'action' => 'unbind',
            'plugin_appid' => $appId,
        ]);
    }


    /**
     * 获取当前所有插件使用方（供插件开发者调用）
     *
     * @https://developers.weixin.qq.com/miniprogram/dev/api-backend/getPluginDevApplyList.html
     *
     * @param int $page
     * @param int $num
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getDevApplyList(int $page = 1, int $num = 10)
    {
        return $this->httpPostJson('wxa/devplugin', [
            'action' => 'list',
            'page' => $page,
            'num' => $num,
        ]);
    }


    public function setDevApplyStatus(string $appId, string $action ='dev_agree', string $reason = null)
    {
        if (!in_array($action, ['dev_agree', 'dev_refuse','dev_delete'], true)) {
            throw new InvalidArgumentException('status should be 0 or 1.');
        }
        return $this->httpPostJson('wxa/devplugin', [
            'action' => $action,
            'appId' => $appId,
            'reason' => $reason,
        ]);
    }
}
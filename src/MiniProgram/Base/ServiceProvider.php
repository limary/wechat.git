<?php
namespace Sinta\Wechat\MiniProgram\Base;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 认证服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\MiniProgram\Auth
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {

        $app['base'] = function ($app) {
            return new Client($app);
        };
    }
}
<?php
namespace Sinta\Wechat\MiniProgram;

use Sinta\Wechat\BasicService;
use Sinta\Wechat\Kernel\ServiceContainer;


/**
 * 小程序应用
 *
 * Class Application
 * @package Sinta\Wechat\MiniProgram
 */
class Application extends ServiceContainer
{

    protected $providers = [
        Auth\ServiceProvider::class,
        DataCube\ServiceProvider::class,
        AppCode\ServiceProvider::class,
        Server\ServiceProvider::class,
        TemplateMessage\ServiceProvider::class,
        CustomerService\ServiceProvider::class,
        UniformMessage\ServiceProvider::class,

        BasicService\Media\ServiceProvider::class,
        BasicService\ContentSecurity\ServiceProvider::class,
        OpenData\ServiceProvider::class,
        Plugin\ServiceProvider::class,
        Base\ServiceProvider::class,
        Express\ServiceProvider::class,
        NearbyPoi\ServiceProvider::class,
    ];
}
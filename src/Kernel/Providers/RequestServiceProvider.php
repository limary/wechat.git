<?php
namespace Sinta\Wechat\Kernel\Providers;


use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * 请求服务
 *
 * Class RequestServiceProvider
 * @package Sinta\Wechat\Kernel\Providers
 */
class RequestServiceProvider implements ServiceProviderInterface
{
    public function register(Container $pimple)
    {
        $pimple['request'] = function(){
            return Request::createFromGlobals();
        };
    }
}
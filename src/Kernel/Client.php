<?php
namespace Sinta\Wechat\Kernel;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Sinta\Wechat\Kernel\Contracts\AccessTokenInterface as AccessTokenContract;
use Sinta\Wechat\Kernel\Traits\HasHttpRequests;
use Sinta\Wechat\Kernel\Http\Response;


class Client
{
    use HasHttpRequests { request as performRequest; }


    protected $app;

    protected $accessToken;


    protected $baseUri;


    /**
     *
     * Client constructor.
     * @param ServiceContainer $app
     * @param AccessTokenContract|null $accessToken
     */
    public function __construct(ServiceContainer $app,AccessTokenContract $accessToken = null)
    {
        $this->app = $app;
        $this->accessToken = $accessToken ?? $this->app['access_token'];
    }

    /**
     * GET请求
     *
     * @param string $url
     * @param array $query
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function httpGet(string $url,array $query = [])
    {
        return $this->request($url,'GET',['query' => $query]);
    }

    /**
     * POST请求
     *
     * @param string $url
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function httpPost(string $url, array $data = [])
    {
        return $this->request($url, 'POST', ['form_params' => $data]);
    }

    /**
     * JSON请求
     * @param string $url
     * @param array $data
     * @param array $query
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function httpPostJson(string $url, array $data = [], array $query = [])
    {
        return $this->request($url, 'POST', ['query' => $query, 'json' => $data]);
    }

    /**
     * 上传文件
     *
     * @param string $url
     * @param array $files
     * @param array $form
     * @param array $query
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function httpUpload(string $url, array $files = [], array $form = [], array $query = [])
    {
        $multipart = [];

        foreach ($files as $name => $path) {
            $multipart[] = [
                'name' => $name,
                'contents' => fopen($path, 'r'),
            ];
        }

        foreach ($form as $name => $contents) {
            $multipart[] = compact('name', 'contents');
        }

        return $this->request($url, 'POST', ['query' => $query, 'multipart' => $multipart]);
    }


    /**
     * 获取AccessToken
     *
     *
     */
    public function getAccessToken(): AccessTokenContract
    {
        return $this->accessToken;
    }

    /**
     * 设置AccessToken
     *
     * @param AccessTokenContract $accessToken
     * @return $this
     */
    public function setAccessToken(AccessTokenContract $accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }


    public function request(string $url,string $method = 'GET',array $options = [],$returnRaw = false)
    {
        if(empty($this->middlewares)){
            $this->registerHttpMiddlewares();
        }
        $response = $this->performRequest($url,$method,$options);

        return $returnRaw ? $response : $this->castResponseToType($response, $this->app->config->get('response_type', 'array'));
    }


    public function requestRaw(string $url, string $method = 'GET', array $options = [])
    {
        return Response::buildFromPsrResponse($this->request($url, $method, $options, true));
    }


    protected function registerHttpMiddlewares()
    {
        // retry
        $this->pushMiddleware($this->retryMiddleware(), 'retry');
        // access token
        $this->pushMiddleware($this->accessTokenMiddleware(), 'access_token');
        // log
        $this->pushMiddleware($this->logMiddleware(), 'log');
    }

    protected function accessTokenMiddleware()
    {
        return function (callable $handler) {
            return function (RequestInterface $request, array $options) use ($handler) {
                if ($this->accessToken) {
                    $request = $this->accessToken->applyToRequest($request, $options);
                }

                return $handler($request, $options);
            };
        };
    }


    protected function logMiddleware()
    {
        $formatter = new MessageFormatter($this->app['config']['http.log_template'] ?? MessageFormatter::DEBUG);

        return Middleware::log($this->app['logger'], $formatter);
    }

    protected function retryMiddleware()
    {
        return Middleware::retry(function (
            $retries,
            RequestInterface $request,
            ResponseInterface $response = null
        ) {
            // Limit the number of retries to 2
            if ($retries < $this->app->config->get('http.max_retries', 1) && $response && $body = $response->getBody()) {
                // Retry on server errors
                $response = json_decode($body, true);

                if (!empty($response['errcode']) && in_array(abs($response['errcode']), [40001, 40014, 42001], true)) {
                    $this->accessToken->refresh();
                    $this->app['logger']->debug('Retrying with refreshed access token.');

                    return true;
                }
            }

            return false;
        }, function () {
            return abs($this->app->config->get('http.retry_delay', 500));
        });
    }

}
<?php
namespace Sinta\Wechat\Kernel;

use Pimple\Container;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Sinta\Wechat\Kernel\Contracts\AccessTokenInterface;
use Sinta\Wechat\Kernel\Exceptions\HttpException;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;
use Sinta\Wechat\Kernel\Traits\HasHttpRequests;
use Sinta\Wechat\Kernel\Traits\InteractsWithCache;


/**
 * Class AccessToken
 * @package Sinta\Wechat\Kernel
 */
abstract class AccessToken implements AccessTokenInterface
{
    use HasHttpRequests, InteractsWithCache;
    /**
     * @var \Pimple\Container
     */
    protected $app;


    /**
     * 请求方法
     *
     * @var string
     */
    protected $requestMethod = 'GET';


    protected $endpointToGetToken;

    /**
     * @var string
     */
    protected $queryName;


    protected $token;


    protected $safeSeconds = 500;


    protected $tokenKey = 'access_token';

    /**
     * 缓存前缀
     *
     * @var string
     */
    protected $cachePrefix = 'sinta-wechat.access_token.';


    /**
     *
     *
     * AccessToken constructor.
     * @param Container $app
     */
    public function __construct(Container $app)
    {
        $this->app = $app;
    }

    public function getRefreshedToken(): array
    {
        return $this->getToken(true);
    }

    /**
     *获取token
     *
     * @param bool $refresh true，时强制从wechat平台拉取
     * @return array
     */
    public function getToken(bool $refresh = false): array
    {
        $cacheKey = $this->getCacheKey();
        $cache = $this->getCache();

        if(!$refresh && $cache->has($cacheKey)){
            return $cache->get($cacheKey);
        }

        $token = $this->requestToken($this->getCredentials());
        $this->setToken($token[$this->tokenKey],$token['expires_in'] ?? 7200);
        return $token;
    }

    /**
     * 设置token到缓存中
     *
     * @param string $token
     * @param int $lifetime
     * @return AccessToken
     */
    public function setToken(string $token,int $lifetime = 7200): self
    {
        $this->getCache()->set($this->getCacheKey(),[
            $this->tokenKey => $token,
            'expires_in' => $lifetime
        ],$lifetime-$this->safeSeconds);

        return $this;
    }


    public function refresh(): AccessTokenInterface
    {
        $this->getToken(true);
        return $this;
    }


    public function requestToken(array $credentials): array
    {
        $result = json_decode($this->sendRequest($credentials)->getBody()->getContents(), true);

        if (empty($result[$this->tokenKey])) {
            throw new HttpException('Request access_token fail: '.json_encode($result, JSON_UNESCAPED_UNICODE));
        }

        return $result;
    }


    public function applyToRequest(RequestInterface $request, array $requestOptions = []): RequestInterface
    {
        parse_str($request->getUri()->getQuery(), $query);
        $query = http_build_query(array_merge($this->getQuery(), $query));

        return $request->withUri($request->getUri()->withQuery($query));
    }

    /**
     * 发送请
     *
     * @param array $credentials
     * @return ResponseInterface
     */
    protected function sendRequest(array $credentials): ResponseInterface
    {
        $options = [
            ($this->requestMethod === 'GET') ? 'query' : 'json' => $credentials,
        ];

        return $this->setHttpClient($this->app['http_client'])->request($this->getEndpoint(), $this->requestMethod, $options);
    }


    protected function getQuery(): array
    {
        return [$this->queryName ?? $this->tokenKey => $this->getToken()[$this->tokenKey]];
    }



    /**
     * 获取缓存key
     *
     * @return string
     */
    protected function getCacheKey()
    {
        return $this->cachePrefix.md5(json_encode($this->getCredentials()));
    }

    public function getEndpoint(): string
    {
        if (empty($this->endpointToGetToken)) {
            throw new InvalidArgumentException('No endpoint for access token request.');
        }

        return $this->endpointToGetToken;
    }


    abstract protected function getCredentials(): array;
}
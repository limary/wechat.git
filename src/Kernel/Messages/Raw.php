<?php
namespace Sinta\Wechat\Kernel\Messages;


class Raw extends Message
{
    protected $type = 'raw';

    protected $properties = ['content'];

    public function __construct(string $content)
    {
        parent::__construct(['content' => strval($content)]);
    }

    public function transformForJsonRequest(array $appends = [], $withType = true): array
    {
        return json_decode($this->content, true) ?? [];
    }

    public function __toString()
    {
        return $this->get('content') ?? '';
    }
}
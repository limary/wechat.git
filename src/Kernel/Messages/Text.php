<?php
namespace Sinta\Wechat\Kernel\Messages;

/**
 * 文本消息
 *
 * Class Text
 * @package Sinta\Wechat\Kernel\Messages
 */
class Text extends Message
{

    protected $type = 'text';

    protected $properties = ['content'];

    public function __construct(string $content)
    {
        parent::__construct(compact('content'));
    }


    public function toXmlArray()
    {
        return [
            'Content' => $this->get('content'),
        ];
    }
}
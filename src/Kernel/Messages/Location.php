<?php

namespace Sinta\Wechat\Kernel\Messages;

/**
 * 地理消息
 *
 * Class Location
 * @package Sinta\Wechat\Kernel\Messages
 */
class Location extends Message
{
    protected $type = 'location';

    protected $properties = [
        'latitude',
        'longitude',
        'scale',
        'label',
        'precision',
    ];
}
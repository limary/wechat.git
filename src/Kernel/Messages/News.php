<?php
namespace Sinta\Wechat\Kernel\Messages;

/**
 * 新闻消息
 *
 * Class News
 * @package Sinta\Wechat\Kernel\Messages
 */
class News extends Message
{
    protected $type = 'news';

    /**
     * 属性设置
     *
     * @var array
     */
    protected $properties = [
        'items',
    ];

    public function __construct(array $items = [])
    {
        parent::__construct(compact('items'));
    }


    public function propertiesToArray(array $data, array $aliases = []): array
    {
        return ['articles' => array_map(function ($item) {
            if ($item instanceof NewsItem) {
                return $item->toJsonArray();
            }
        }, $this->get('items'))];
    }

    public function toXmlArray()
    {
        $items = [];

        foreach ($this->get('items') as $item) {
            if ($item instanceof NewsItem) {
                $items[] = $item->toXmlArray();
            }
        }

        return [
            'ArticleCount' => count($items),
            'Articles' => $items,
        ];
    }

}
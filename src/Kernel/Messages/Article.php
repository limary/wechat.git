<?php
namespace Sinta\Wechat\Kernel\Messages;


class Article extends Message
{

    protected $type = 'mpnews';

    protected $properties = [
        'thumb_media_id',
        'author',
        'title',
        'content',
        'digest',
        'source_url',
        'show_cover',
    ];

    protected $jsonAliases = [
        'content_source_url' => 'source_url',
        'show_cover_pic' => 'show_cover',
    ];

    protected $required = [
        'thumb_media_id',
        'title',
        'content',
        'source_url',
        'show_cover',
    ];
}
<?php
namespace Sinta\Wechat\Kernel\Messages;


class Transfer extends Message
{
    protected $type = 'transfer_customer_service';

    protected $properties = [
        'account',
    ];

    public function __construct(string $account = null)
    {
        parent::__construct(compact('account'));
    }

    public function toXmlArray()
    {
        return empty($this->get('account')) ? [] : [
            'TransInfo' => [
                'KfAccount' => $this->get('account'),
            ],
        ];
    }
}
<?php
namespace Sinta\Wechat\Kernel\Messages;


class TextCard extends Message
{
    protected $type = 'textcard';

    protected $properties = [
        'title',
        'description',
        'url',
    ];
}
<?php
namespace Sinta\Wechat\Kernel\Messages;

/**
 * 语音消息
 *
 * Class Voice
 * @package Sinta\Wechat\Kernel\Messages
 */
class Voice extends Message
{
    protected $type = 'voice';

    /**
     * 属性
     *
     * @var array
     */
    protected $properties = [
        'media_id',
        'recognition',
    ];
}
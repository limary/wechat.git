<?php
namespace Sinta\Wechat\Kernel\Messages;


class Link extends Message
{
    protected $type = 'link';

    protected $properties = [
        'title',
        'description',
        'url',
    ];
}
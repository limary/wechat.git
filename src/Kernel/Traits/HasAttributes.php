<?php
namespace Sinta\Wechat\Kernel\Traits;

use Sinta\Wechat\Kernel\Support\Arr;
use Sinta\Wechat\Kernel\Support\Str;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;

/**
 * 属性
 *
 * Class HasAttributes
 * @package Sinta\Wechat\Kernel\Traits
 */
trait HasAttributes
{
    protected $attributes = [];

    protected $snakeable = true;

    /**
     * 设置属性
     *
     * @param array $attributes
     * @return $this
     */
    public function setAttributes(array $attributes = [])
    {
        $this->attributes = $attributes;

        return $this;
    }


    public function setAttribute($attribute, $value)
    {
        Arr::set($this->attributes, $attribute, $value);

        return $this;
    }

    /**
     * 获取属性
     *
     * @param $attribute
     * @param null $default
     * @return mixed
     */
    public function getAttribute($attribute, $default = null)
    {
        return Arr::get($this->attributes, $attribute, $default);
    }


    public function isRequired($attribute)
    {
        return in_array($attribute, $this->getRequired(), true);
    }

    public function getRequired()
    {
        return property_exists($this, 'required') ? $this->required : [];
    }


    public function with($attribute, $value)
    {
        $this->snakeable && $attribute = Str::snake($attribute);

        $this->setAttribute($attribute, $value);

        return $this;
    }


    public function set($attribute, $value)
    {
        $this->setAttribute($attribute, $value);

        return $this;
    }


    public function get($attribute, $default = null)
    {
        return $this->getAttribute($attribute, $default);
    }

    public function has(string $key)
    {
        return Arr::has($this->attributes, $key);
    }


    public function merge(array $attributes)
    {
        $this->attributes = array_merge($this->attributes, $attributes);

        return $this;
    }

    public function only($keys)
    {
        return Arr::only($this->attributes, $keys);
    }


    /**
     * 获取所有属性
     *
     * @return array
     */
    public function all()
    {
        $this->checkRequiredAttributes();

        return $this->attributes;
    }



    public function __call($method,$args)
    {
        if (stripos($method, 'with') === 0) {
            return $this->with(substr($method, 4), array_shift($args));
        }
        throw new \BadMethodCallException(sprintf('Method "%s" does not exists.', $method));
    }


    /**
     * 获取属性
     *
     * @param $property
     * @return mixed
     */
    public function __get($property)
    {
        return $this->get($property);
    }


    /**
     * 设置属性
     *
     * @param $property
     * @param $value
     * @return $this
     */
    public function __set($property, $value)
    {
        return $this->with($property, $value);
    }



    /**
     * 是否有这个属性
     *
     * @param $key
     * @return bool
     */
    public function __isset($key)
    {
        return isset($this->attributes[$key]);
    }


    /**
     * 检测必在的属性
     *
     * @throws InvalidArgumentException
     */
    protected function checkRequiredAttributes()
    {
        foreach ($this->getRequired() as $attribute) {
            if (is_null($this->get($attribute))) {
                throw new InvalidArgumentException(sprintf('"%s" cannot be empty.', $attribute));
            }
        }
    }
}
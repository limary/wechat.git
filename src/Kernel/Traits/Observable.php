<?php
namespace Sinta\Wechat\Kernel\Traits;

use Closure;
use Sinta\Wechat\Kernel\Contracts\EventHandlerInterface;
use Sinta\Wechat\Kernel\Decorators\FinallyResult;
use Sinta\Wechat\Kernel\Decorators\TerminateResult;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;
use Sinta\Wechat\Kernel\ServiceContainer;

/**
 * 观察者
 *
 * Class Observable
 * @package Sinta\Wechat\Kernel\Traits
 */
trait Observable
{
    /**
     * @var array
     */
    protected $handlers = [];

    protected $clauses = [];


    /**
     *
     *
     * @param $handler
     * @param string $condition
     */
    public function push($handler,$condition = '*')
    {
        list($handler, $condition) = $this->resolveHandlerAndCondition($handler, $condition);

        if (!isset($this->handlers[$condition])) {
            $this->handlers[$condition] = [];
        }

        array_push($this->handlers[$condition], $handler);

        return $this->newClause($handler);
    }


    public function unshift($handler, $condition = '*')
    {
        list($handler, $condition) = $this->resolveHandlerAndCondition($handler, $condition);

        if (!isset($this->handlers[$condition])) {
            $this->handlers[$condition] = [];
        }

        array_unshift($this->handlers[$condition], $handler);

        return $this->newClause($handler);
    }


    public function observe($condition, $handler)
    {
        $this->push($handler, $condition);
    }


    public function on($condition, $handler)
    {
        $this->push($handler, $condition);
    }


    public function dispatch($event, $payload)
    {
        return $this->notify($event, $payload);
    }

    public function notify($event, $payload)
    {
        $result = null;

        foreach ($this->handlers as $condition => $handlers) {
            if ($condition === '*' || ($condition & $event) === $event) {
                foreach ($handlers as $handler) {
                    $response = $this->callHandler($handler, $payload);

                    switch (true) {
                        case $response instanceof TerminateResult:
                            return $response->content;
                        case true === $response:
                            continue 2;
                        case false === $response:
                            break 2;
                        case !empty($response) && !($result instanceof FinallyResult):
                            $result = $response;
                    }
                }
            }
        }

        return $result instanceof FinallyResult ? $result->content : $result;
    }


    public function getHandlers()
    {
        return $this->handlers;
    }

    protected function newClause($handler): Clause
    {
        return $this->clauses[spl_object_hash((object) $handler)] = new Clause();
    }


    protected function callHandler(Closure $handler, array $payload)
    {
        try {
            return $handler($payload);
        } catch (\Exception $e) {
            if (property_exists($this, 'app') && $this->app instanceof ServiceContainer) {
                $this->app['logger']->error($e->getCode().': '.$e->getMessage(), [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                ]);
            }
        }
    }


    protected function makeClosure($handler)
    {
        if (is_callable($handler)) {
            return $handler;
        }

        if (is_string($handler)) {
            if (!class_exists($handler)) {
                throw new InvalidArgumentException(sprintf('Class "%s" not exists.', $handler));
            }

            if (!in_array(EventHandlerInterface::class, (new \ReflectionClass($handler))->getInterfaceNames(), true)) {
                throw new InvalidArgumentException(sprintf('Class "%s" not an instance of "%s".', $handler, IEventHandler::class));
            }

            return function ($payload) use ($handler) {
                return (new $handler($this->app ?? null))->handle($payload);
            };
        }

        if ($handler instanceof EventHandlerInterface) {
            return function () use ($handler) {
                return $handler->handle(...func_get_args());
            };
        }

        throw new InvalidArgumentException('No valid handler is found in arguments.');
    }

    /**
     * @param $handler
     * @param $condition
     *
     * @return array
     */
    protected function resolveHandlerAndCondition($handler, $condition): array
    {
        if (is_int($handler) || (is_string($handler) && !class_exists($handler))) {
            list($handler, $condition) = [$condition, $handler];
        }

        return [$this->makeClosure($handler), $condition];
    }
}
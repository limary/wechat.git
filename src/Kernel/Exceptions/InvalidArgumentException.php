<?php
namespace Sinta\Wechat\Kernel\Exceptions;

/**
 * 无效参数异常
 *
 * Class InvalidArgumentException
 * @package Sinta\Wechat\Kernel\Exceptions
 */
class InvalidArgumentException extends Exception
{

}
<?php
namespace Sinta\Wechat\Kernel;

use Pimple\Container;



/**
 * 服务容器
 *
 * Class ServiceContainer
 * @package Sinta\Wechat\Kernel
 */
class ServiceContainer extends Container
{
    use WithAggregator;

    /**
     * @var string
     */
    protected $id;


    /**
     * 提供者
     *
     * @var array
     */
    protected $providers = [];


    /**
     * 默认设置
     *
     * @var array
     */
    protected $defaultConfig = [];

    //用户设置
    protected $userConfig = [];


    /**
     * 创一个服务容器
     *
     * ServiceContainer constructor.
     * @param array $config
     * @param array $prepends
     */
    public function __construct(array $config = [],array $prepends = [], string $id = null)
    {
        $this->registerProviders($this->getProviders());

        parent::__construct($prepends);

        $this->userConfig = $config;

        $this->id = $id;
    }


    public function getId()
    {
        return $this->id ?? $this->id = md5(json_encode($this->userConfig));
    }


    public function getConfig()
    {
        $base = [
            'http' => [
                'timeout' => 5.0,
                'base_uri' => 'https://api.weixin.qq.com/',
            ],
        ];
        return array_replace_recursive($base, $this->defaultConfig, $this->userConfig);
    }

    /**
     * 返回注册的服务
     *
     * @return array
     */
    public function getProviders()
    {
        return array_merge([
            Providers\ConfigServiceProvider::class,
            Providers\LogServiceProvider::class,
            Providers\RequestServiceProvider::class,
            Providers\HttpClientServiceProvider::class,
            Providers\ExtensionServiceProvider::class,
        ],$this->providers);
    }


    public function __get($id)
    {
        return $this->offsetGet($id);
    }


    public function __set($id, $value)
    {
        $this->offsetSet($id, $value);
    }


    public function registerProviders(array $providers)
    {
        foreach ($providers as $provider) {
            parent::register(new $provider());
        }
    }
}
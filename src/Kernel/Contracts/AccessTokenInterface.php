<?php
namespace Sinta\Wechat\Kernel\Contracts;

use Psr\Http\Message\RequestInterface;

/**
 * 访问Token接口
 *
 * Interface IAccessToken
 * @package Sinta\Wechat\Kernel\Contract
 */
interface AccessTokenInterface
{
    public function getToken(): array;

    public function refresh(): self;

    public function applyToRequest(RequestInterface $request, array $requestOptions = []): RequestInterface;
}
<?php
namespace Sinta\Wechat\Kernel\Http;

use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Psr\Http\Message\ResponseInterface;
use Sinta\Wechat\Kernel\Support\XML;
use Sinta\Wechat\Kernel\Support\Collection;

/**
 *
 *
 * Class Response
 * @package Sinta\Wechat\Kernel\Http
 */
class Response extends GuzzleResponse
{
    /**
     * 获取body content内容
     *
     * @return string
     */
    public function getBodyContents()
    {
        $this->getBody()->rewind();
        $contents = $this->getBody()->getContents();
        $this->getBody()->rewind();

        return $contents;
    }

    public static function buildFromPsrResponse(ResponseInterface $response)
    {
        return new static(
            $response->getStatusCode(),
            $response->getHeaders(),
            $response->getBody(),
            $response->getProtocolVersion(),
            $response->getReasonPhrase()
        );
    }

    public function toJson()
    {
        return json_encode($this->toArray());
    }



    public function toArray()
    {
        $content = $this->getBodyContents();

        if (false !== stripos($this->getHeaderLine('Content-Type'), 'xml') || stripos($content, '<xml') === 0) {
            return XML::parse($content);
        }

        $array = json_decode($this->getBodyContents(), true);

        if (JSON_ERROR_NONE === json_last_error()) {
            return $array;
        }

        return [];
    }

    public function toCollection()
    {
        return new Collection($this->toArray());
    }

    public function toObject()
    {
        return json_decode($this->getBodyContents());
    }


    public function __toString()
    {
        return $this->getBodyContents();
    }
}
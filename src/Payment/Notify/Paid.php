<?php

namespace Sinta\Wechat\Payment\Notify;

use Closure;

/**
 * 支付能知处理
 *
 * Class Paid
 * @package Sinta\Wechat\Payment\Notify
 */
class Paid extends Handler
{
    public function handle(Closure $closure)
    {
        $this->strict(
            call_user_func($closure,$this->getMessage(),[$this,'fail'])
        );
        return $this->toResponse();
    }

}
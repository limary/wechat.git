<?php
namespace Sinta\Wechat\Payment\Security;

use Sinta\Wechat\Payment\Kernel\BaseClient;

class Client extends BaseClient
{
    public function getPublicKey()
    {
        $params = [
            'sign_type' => 'MD5',
        ];
        return $this->safeRequest('https://fraud.mch.weixin.qq.com/risk/getpublickey', $params);
    }
}

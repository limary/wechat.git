<?php
namespace Sinta\Wechat\Payment\Redpack;

use Sinta\Wechat\Kernel\Support;
use Sinta\Wechat\Payment\Kernel\BaseClient;

/**
 * 红包服务方法
 *
 * Class Client
 * @package Sinta\Wechat\Payment\Redpack
 */
class Client extends BaseClient
{
    /**
     * 查询红包记录
     *
     * 用于商户对已发放的红包进行查询红包的具体信息，可支持普通红包和裂变包。
     *
     * @param  $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/tools/cash_coupon.php?chapter=13_6&index=5
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|BaseClient
     */
    public function query($mchBillno)
    {
        $params = is_array($mchBillno) ? $mchBillno : ['mch_billno' => $mchBillno];
        $base = [
            'appid' => $this->app['config']->app_id,
            'bill_type' => 'MCHT',
        ];
        return $this->safeRequest('mmpaymkttransfers/gethbinfo', array_merge($base, $params));
    }

    /**
     * 发放普通红包
     *
     * @param array $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/tools/cash_coupon.php?chapter=13_4&index=3
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|BaseClient
     */
    public function sendNormal(array $params)
    {
        $base = [
            'total_num' => 1,
            'client_ip' => $params['client_ip'] ?? Support\get_server_ip(),
            'wxappid' => $this->app['config']->app_id,
        ];

        return $this->safeRequest('mmpaymkttransfers/sendredpack', array_merge($base, $params));
    }

    /**
     * 发放裂变红包
     *
     * @param array $params
     * @see https://pay.weixin.qq.com/wiki/doc/api/tools/cash_coupon.php?chapter=13_5&index=4
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|BaseClient
     */
    public function sendGroup(array $params)
    {
        $base = [
            'amt_type' => 'ALL_RAND',
            'wxappid' => $this->app['config']->app_id,
        ];

        return $this->safeRequest('mmpaymkttransfers/sendgroupredpack', array_merge($base, $params));
    }

}
<?php

namespace Sinta\Wechat\Payment\Redpack;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 红包服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\Payment\Redpack
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['redpack'] = function ($app) {
            return new Client($app);
        };
    }
}
<?php

namespace Sinta\Wechat\Payment\Reverse;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 撤销订单服务
 *
 * Class ServiceProvider
 * @see https://pay.weixin.qq.com/wiki/doc/api/micropay_sl_jw.php?chapter=9_11&index=3
 * @package Sinta\Wechat\Payment\Reverse
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['reverse'] = function ($app){
            return new Client($app);
        };
    }
}
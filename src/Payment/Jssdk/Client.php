<?php
namespace Sinta\Wechat\Payment\Jssdk;


use Sinta\Wechat\BasicService\Jssdk\Client as JssdkClient;
use Sinta\Wechat\Kernel\Support;
use Sinta\Socialite\AccessTokenInterface;

class Client extends JssdkClient
{
    public function bridgeConfig($prepayId,$json = true)
    {
        $params = [
            'appId' => $this->app['config']->sub_appid ?: $this->app['config']->app_id,
            'timeStamp' => strval(time()),
            'nonceStr' => uniqid(),
            'package' => "prepay_id=$prepayId",
            'signType' => 'MD5',
        ];
        $params['paySign'] = Support\generate_sign($params, $this->app['config']->key, 'md5');
        return $json ? json_encode($params) : $params;
    }

    public function sdkConfig($prepayId)
    {
        $config = $this->bridgeConfig($prepayId, false);
        $config['timestamp'] = $config['timeStamp'];
        unset($config['timeStamp']);
        return $config;
    }

    public function appConfig($prepayId)
    {
        $params = [
            'appid' => $this->app['config']->app_id,
            'partnerid' => $this->app['config']->mch_id,
            'prepayid' => $prepayId,
            'noncestr' => uniqid(),
            'timestamp' => time(),
            'package' => 'Sign=WXPay',
        ];
        $params['sign'] = Support\generate_sign($params, $this->app['config']->key);
        return $params;
    }


    public function shareAddressConfig($accessToken, $json = true)
    {
        if ($accessToken instanceof AccessTokenInterface) {
            $accessToken = $accessToken->getToken();
        }
        $params = [
            'appId' => $this->app['config']->app_id,
            'scope' => 'jsapi_address',
            'timeStamp' => strval(time()),
            'nonceStr' => uniqid(),
            'signType' => 'SHA1',
        ];
        $signParams = [
            'appid' => $params['appId'],
            'url' => $this->getUrl(),
            'timestamp' => $params['timeStamp'],
            'noncestr' => $params['nonceStr'],
            'accesstoken' => strval($accessToken),
        ];
        ksort($signParams);
        $params['addrSign'] = sha1(urldecode(http_build_query($signParams)));
        return $json ? json_encode($params) : $params;
    }

}
<?php
namespace Sinta\Wechat\OpenWork\Provider;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\ServiceContainer;

class Client extends BaseClient
{

    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app, $app['provider_access_token']);
    }

    /**
     * 单点登录 - 获取登录的地址.
     *
     * @param string $redirectUri
     * @param string $userType
     * @param string $state
     * @return string
     */
    public function getLoginUrl(string $redirectUri = '', string $userType = 'admin', string $state = '')
    {
        $redirectUri || $redirectUri = $this->app->config['redirect_uri_single'];
        $state || $state = rand();
        $params = [
            'appid' => $this->app['config']['corp_id'],
            'redirect_uri' => $redirectUri,
            'usertype' => $userType,
            'state' => $state,
        ];
        return 'https://open.work.weixin.qq.com/wwopen/sso/3rd_qrConnect?'.http_build_query($params);
    }

    /**
     * 单点登录 - 获取登录用户信息.
     *
     * @param string $authCode
     * @return mixed
     */
    public function getLoginInfo(string $authCode)
    {
        $params = [
            'auth_code' => $authCode,
        ];
        return $this->httpPostJson('cgi-bin/service/get_login_info', $params);
    }

    /**
     * 获取注册定制化URL.
     *
     * @param string $registerCode
     * @return string
     */
    public function getRegisterUri(string $registerCode = '')
    {
        $registerCode || $registerCode = $this->getRegisterCode()['register_code'];
        $params = ['register_code' => $registerCode];
        return 'https://open.work.weixin.qq.com/3rdservice/wework/register?'.http_build_query($params);
    }

    /**
     * 获取注册码.
     *
     * @param string $corpName
     * @param string $adminName
     * @param string $adminMobile
     * @param string $state
     * @return mixed
     */
    public function getRegisterCode(
        string $corpName = '',
        string $adminName = '',
        string $adminMobile = '',
        string $state = ''
    ) {
        $params = [];
        $params['template_id'] = $this->app['config']['reg_template_id'];
        !empty($corpName) && $params['corp_name'] = $corpName;
        !empty($adminName) && $params['admin_name'] = $adminName;
        !empty($adminMobile) && $params['admin_mobile'] = $adminMobile;
        !empty($state) && $params['state'] = $state;
        return $this->httpPostJson('cgi-bin/service/get_register_code', $params);
    }

    /**
     * 查询注册状态.
     *
     * @param string $registerCode
     * @return mixed
     */
    public function getRegisterInfo(string $registerCode)
    {
        $params = [
            'register_code' => $registerCode,
        ];
        return $this->httpPostJson('cgi-bin/service/get_register_info', $params);
    }

    /**
     * 设置授权应用可见范围.
     *
     * @param string $accessToken
     * @param string $agentId
     * @param array $allowUser
     * @param array $allowParty
     * @param array $allowTag
     * @return mixed
     */
    public function setAgentScope(
        string $accessToken,
        string $agentId,
        array $allowUser = [],
        array $allowParty = [],
        array $allowTag = []
    ) {
        $params = [
            'agentid' => $agentId,
            'allow_user' => $allowUser,
            'allow_party' => $allowParty,
            'allow_tag' => $allowTag,
            'access_token' => $accessToken,
        ];
        return $this->httpGet('cgi-bin/agent/set_scope', $params);
    }


    public function contactSyncSuccess(string $accessToken)
    {
        $params = ['access_token' => $accessToken];
        return $this->httpGet('cgi-bin/sync/contact_sync_success', $params);
    }


}
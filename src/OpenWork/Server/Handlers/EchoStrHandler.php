<?php

namespace Sinta\Wechat\OpenWork\Server\Handlers;

use Sinta\Wechat\Kernel\Contracts\EventHandlerInterface;
use Sinta\Wechat\Kernel\Decorators\FinallyResult;
use Sinta\Wechat\Kernel\ServiceContainer;

class EchoStrHandler implements EventHandlerInterface
{
    protected $app;

    public function __construct(ServiceContainer $app)
    {
        $this->app = $app;
    }

    public function handle($payload = null)
    {
        if ($decrypted = $this->app['request']->get('echostr')) {
            $str = $this->app['encryptor_corp']->decrypt(
                $decrypted,
                $this->app['request']->get('msg_signature'),
                $this->app['request']->get('nonce'),
                $this->app['request']->get('timestamp')
            );
            return new FinallyResult($str);
        }
        //把SuiteTicket缓存起来
        if (!empty($payload['SuiteTicket'])) {
            $this->app['suite_ticket']->setTicket($payload['SuiteTicket']);
        }
    }
}
<?php
namespace Sinta\Wechat\OpenWork\SuiteAuth;


use Sinta\Wechat\Kernel\Exceptions\RuntimeException;
use Sinta\Wechat\Kernel\Traits\InteractsWithCache;
use Sinta\Wechat\OpenWork\Application;

class SuiteTicket
{
    use InteractsWithCache;

    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $ticket
     * @return $this
     * @throws RuntimeException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function setTicket(string $ticket)
    {
        $this->getCache()->set($this->getCacheKey(), $ticket, 1000);

        if (!$this->getCache()->has($this->getCacheKey())) {
            throw new RuntimeException('Failed to cache suite ticket.');
        }

        return $this;
    }

    /**
     * @return string
     * @throws RuntimeException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getTicket(): string
    {
        if ($cached = $this->getCache()->get($this->getCacheKey())) {
            return $cached;
        }
        throw new RuntimeException('Credential "suite_ticket" does not exist in cache.');
    }

    protected function getCacheKey(): string
    {
        return 'sintawechat.open_work.suite_ticket.'.$this->app['config']['suite_id'];
    }
}
<?php
namespace Sinta\Wechat\BasicService\ContentSecurity;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 内容安全服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\BasicService\ContentSecurity
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['content_security'] = function ($app) {
            return new Client($app);
        };
    }
}
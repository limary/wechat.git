<?php
namespace Sinta\Wechat\BasicService\Jssdk;

use Pimple\Container;
use Pimple\ServiceProviderInterface;


class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        isset($app['jssdk']) || $app['jssdk'] = function ($app){
            return new Client($app);
        };
    }
}
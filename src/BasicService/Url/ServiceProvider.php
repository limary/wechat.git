<?php
namespace Sinta\Wechat\BasicService\Url;


use Pimple\Container;
use Pimple\ServiceProviderInterface;


/**
 * Url服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\BaseService\Qrcode
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        isset($app['url']) || $app['url'] = function ($app){
            return new Client($app);
        };
    }
}
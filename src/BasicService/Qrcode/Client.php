<?php
namespace Sinta\Wechat\BasicService\Qrcode;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 为了满足用户渠道推广分析和用户帐号绑定等场景的需要，
 * 公众平台提供了生成带参数二维码的接口。使用该接口可以获得多个带不同场景值的二维码，
 * 用户扫描后，公众号可以接收到事件推送。
 *
 * 目前有2种类型的二维码：

1、临时二维码，是有过期时间的，最长可以设置为在二维码生成后的30天（即2592000秒）后过期，
 * 但能够生成较多数量。临时二维码主要用于帐号绑定等不要求二维码永久保存的业务场景
2、永久二维码，是无过期时间的，但数量较少（目前为最多10万个）。永久二维码主要用于适用于帐号绑定、用户来源统计等场景。

用户扫描带场景值二维码时，可能推送以下两种事件：

如果用户还未关注公众号，则用户可以关注公众号，关注后微信会将带场景值关注事件推送给开发者。

如果用户已经关注公众号，在用户扫描后会自动进入会话，微信也会将带场景值扫描事件推送给开发者。
 *
 * Class Client
 * @package Sinta\Wechat\BasicService\Qrcode
 */
class Client extends BaseClient
{
    protected $baseUri = 'https://api.weixin.qq.com/cgi-bin/';

    const DAY = 86400;
    const SCENE_MAX_VALUE = 100000;
    const SCENE_QR_CARD = 'QR_CARD';
    const SCENE_QR_TEMPORARY = 'QR_SCENE';
    const SCENE_QR_TEMPORARY_STR = 'QR_STR_SCENE';
    const SCENE_QR_FOREVER = 'QR_LIMIT_SCENE';
    const SCENE_QR_FOREVER_STR = 'QR_LIMIT_STR_SCENE';

    /**
     * 永久二维码
     *
     * @param $sceneValue
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function forever($sceneValue)
    {
        if (is_int($sceneValue) && $sceneValue > 0 && $sceneValue < self::SCENE_MAX_VALUE) {
            $type = self::SCENE_QR_FOREVER;
            $sceneKey = 'scene_id';//场景值ID，临时二维码时为32位非0整型，永久二维码时最大值为100000（目前参数只支持1--100000）
        } else {
            $type = self::SCENE_QR_FOREVER_STR;
            $sceneKey = 'scene_str';//场景值ID（字符串形式的ID），字符串类型，长度限制为1到64
        }
        $scene = [$sceneKey => $sceneValue];

        return $this->create($type, $scene, false);
    }

    /**
     * 临时二维码
     *
     * @param $sceneValue
     * @param null $expireSeconds
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function temporary($sceneValue, $expireSeconds = null)
    {
        if (is_int($sceneValue) && $sceneValue > 0) {
            $type = self::SCENE_QR_TEMPORARY;
            $sceneKey = 'scene_id';
        } else {
            $type = self::SCENE_QR_TEMPORARY_STR;
            $sceneKey = 'scene_str';
        }
        $scene = [$sceneKey => $sceneValue];

        return $this->create($type, $scene, true, $expireSeconds);
    }

    /**
     * 通过ticket换取二维码
     *
     * @param $ticket
     * @return string
     */
    public function url($ticket)
    {
        return sprintf('https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=%s', urlencode($ticket));
    }


    protected function create($actionName, $actionInfo, $temporary = true, $expireSeconds = null)
    {
        $expireSeconds !== null || $expireSeconds = 7 * self::DAY;

        $params = [
            'action_name' => $actionName,
            'action_info' => ['scene' => $actionInfo],
        ];

        if ($temporary) {
            $params['expire_seconds'] = min($expireSeconds, 30 * self::DAY);
        }

        return $this->httpPostJson('qrcode/create', $params);
    }
}
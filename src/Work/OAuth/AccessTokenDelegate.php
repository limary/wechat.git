<?php
namespace Sinta\Wechat\Work\OAuth;

use Sinta\Wechat\Work\Application;
use Sinta\Socialite\AccessTokenInterface;

class AccessTokenDelegate implements AccessTokenInterface
{
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getToken()
    {
        return $this->app['access_token']->getToken()['access_token'];
    }
}
<?php
namespace Sinta\Wechat\Work\OAuth;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Sinta\Socialite\SocialiteManager;

class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['oauth'] = function ($app){
            $socialite = (new SocialiteManager([
                'wework' => [
                    'client_id' => $app['config']['corp_id'],
                    'client_secret' => null,
                    'redirect' => null,
                ]
            ],$app['request']))->driver('wework');

            $scopes = (array) $app['config']->get('oauth.scopes', ['snsapi_base']);

            if (!empty($scopes)) {
                $socialite->scopes($scopes);
            } else {
                $socialite->setAgentId($app['config']['agent_id']);
            }

            return $socialite->setAccessToken(new AccessTokenDelegate($app));
        };
    }

    private function prepareCallbackUrl($app)
    {
        $callback = $app['config']->get('oauth.callback');
        if (0 === stripos($callback, 'http')) {
            return $callback;
        }
        $baseUrl = $app['request']->getSchemeAndHttpHost();
        return $baseUrl.'/'.ltrim($callback, '/');
    }
}
<?php
namespace Sinta\Wechat\Work\Server;

use Sinta\Wechat\Kernel\ServerGuard;

/**
 * Class Guard
 *
 * @package Sinta\Wechat\Work\Server
 */
class Guard extends ServerGuard
{
    protected $alwaysValidate = true;

    public function validate()
    {
        return $this;
    }

    public function isSafeMode(): bool
    {
        return true;
    }


    protected function shouldReturnRawResponse(): bool
    {
        return !is_null($this->app['request']->get('echostr'));
    }
}
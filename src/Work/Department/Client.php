<?php
namespace Sinta\Wechat\Work\Department;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 部门管理
 *
 * Class Client
 * @package Sinta\Wechat\Work\Department
 */
class Client extends BaseClient
{
    /**
     *
     *
     * @param array $data
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(array $data)
    {
        return $this->httpPostJson('cgi-bin/department/create', $data);
    }

    public function update(int $id, array $data)
    {
        return $this->httpPostJson('cgi-bin/department/update', array_merge(compact('id'), $data));
    }


    public function delete($id)
    {
        return $this->httpGet('cgi-bin/department/delete', compact('id'));
    }

    public function list($id = null)
    {
        return $this->httpGet('cgi-bin/department/list', compact('id'));
    }


}
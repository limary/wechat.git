<?php
namespace Sinta\Wechat\Work\MiniProgram;

use Sinta\Wechat\MiniProgram\Application as MiniProgram;
use Sinta\Wechat\Work\Auth\AccessToken;
use Sinta\Wechat\Work\MiniProgram\Auth\Client;

class Application extends MiniProgram
{
    public function __construct(array $config = [], array $prepends = [])
    {
        parent::__construct($config, $prepends + [
            'access_token' => function($app){
                return new AccessToken($app);
            },
            'auth' => function($app){
                return new Client($app);
            }
        ]);
    }
}
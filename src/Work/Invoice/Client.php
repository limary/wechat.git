<?php
namespace Sinta\Wechat\Work\Invoice;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 发票
 *
 * Class Client
 * @see https://work.weixin.qq.com/api/doc#11631
 * @package Sinta\Wechat\Work\Department
 */
class Client extends BaseClient
{
    public function get(string $cardId, string $encryptCode)
    {
        $params = [
            'card_id' => $cardId,
            'encrypt_code' => $encryptCode,
        ];
        return $this->httpPostJson('cgi-bin/card/invoice/reimburse/getinvoiceinfo', $params);
    }


    public function select(array $invoices)
    {
        $params = [
            'item_list' => $invoices,
        ];
        return $this->httpPostJson('cgi-bin/card/invoice/reimburse/getinvoiceinfobatch', $params);
    }

    public function update(string $cardId, string $encryptCode, string $status)
    {
        $params = [
            'card_id' => $cardId,
            'encrypt_code' => $encryptCode,
            'reimburse_status' => $status,
        ];
        return $this->httpPostJson('cgi-bin/card/invoice/reimburse/updateinvoicestatus', $params);
    }

    public function batchUpdate(array $invoices, string $openid, string $status)
    {
        $params = [
            'openid' => $openid,
            'reimburse_status' => $status,
            'invoice_list' => $invoices,
        ];
        return $this->httpPostJson('cgi-bin/card/invoice/reimburse/updatestatusbatch', $params);
    }


}
<?php
namespace Sinta\Wechat\OfficialAccount\ShakeAround;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    public function register($data)
    {
        return $this->httpPostJson('shakearound/account/register',$data);
    }


    public function status()
    {
        return $this->httpGet('shakearound/account/auditstatus');
    }

    public function user(string $ticket, bool $needPoi = false)
    {
        $params = [
            'ticket' => $ticket,
        ];

        if ($needPoi) {
            $params['need_poi'] = 1;
        }

        return $this->httpGet('shakearound/user/getshakeinfo', $params);
    }

    public function userWithPoi(string $ticket)
    {
        return $this->user($ticket, true);
    }
}
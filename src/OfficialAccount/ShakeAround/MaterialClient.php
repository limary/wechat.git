<?php
namespace Sinta\Wechat\OfficialAccount\ShakeAround;

use Sinta\Wechat\Kernel\Client as BaseClient;
use Sinta\Wechat\Kernel\Exceptions\InvalidArgumentException;

class MaterialClient extends BaseClient
{
    public function uploadImage(string $path, string $type = 'icon')
    {
        if (!file_exists($path) || !is_readable($path)) {
            throw new InvalidArgumentException(sprintf('File does not exist, or the file is unreadable: "%s"', $path));
        }

        return $this->httpUpload('shakearound/material/add', ['media' => $path], [], ['type' => strtolower($type)]);
    }
}
<?php
namespace Sinta\Wechat\OfficialAccount\Store;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    public function categories()
    {
        return $this->httpGet('wxa/get_merchant_category');
    }

    public function districts()
    {
        return $this->httpGet('wxa/get_district');
    }

    public function searchFromMap(int $districtId, string $keyword)
    {
        $params = [
            'districtid' => $districtId,
            'keyword' => $keyword,
        ];
        return $this->httpPostJson('wxa/search_map_poi', $params);
    }

    public function getStatus()
    {
        return $this->httpPostJson('wxa/get_merchant_audit_info');
    }

    public function createMerchant(array $baseInfo)
    {
        return $this->httpPostJson('wxa/apply_merchant', $baseInfo);
    }

    public function updateMerchant(array $params)
    {
        return $this->httpPostJson('wxa/modify_merchant', $params);
    }

    public function createFromMap(array $baseInfo)
    {
        return $this->httpPostJson('wxa/create_map_poi', $baseInfo);
    }

    public function create(array $baseInfo)
    {
        return $this->httpPostJson('wxa/add_store', $baseInfo);
    }

    public function update(int $poiId, array $baseInfo)
    {
        $params = array_merge($baseInfo, ['poi_id' => $poiId]);
        return $this->httpPostJson('wxa/update_store', $params);
    }

    public function get(int $poiId)
    {
        return $this->httpPostJson('wxa/get_store_info', ['poi_id' => $poiId]);
    }

    public function list(int $offset = 0, int $limit = 10)
    {
        $params = [
            'offset' => $offset,
            'limit' => $limit,
        ];
        return $this->httpPostJson('wxa/get_store_list', $params);
    }

    public function delete(int $poiId)
    {
        return $this->httpPostJson('wxa/del_store', ['poi_id' => $poiId]);
    }


}
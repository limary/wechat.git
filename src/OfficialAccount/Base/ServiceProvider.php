<?php
namespace Sinta\Wechat\OfficialAccount\Base;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 基础服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount\Base
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
       !isset($app['base']) && $app['base'] = function($app){
            return new Client($app);
       };
    }
}
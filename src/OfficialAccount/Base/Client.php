<?php
namespace Sinta\Wechat\OfficialAccount\Base;

use Sinta\Wechat\Kernel\Client as BaseClient;


class Client extends BaseClient
{
    public function clearQuota()
    {
        $params = [
            'appid' => $this->app['config']['app_id'],
        ];

        return $this->httpPostJson('cgi-bin/clear_quota', $params);
    }

    /**
     * 获取微信服务器IP地址
     *
     * 如果公众号基于安全等考虑，需要获知微信服务器的IP地址列表，
     * 以便进行相关限制，可以通过该接口获得微信服务器IP地址列表或者IP网段信息。
     *
     * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140187
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getValidIps()
    {
        return $this->httpGet('cgi-bin/getcallbackip');
    }
}
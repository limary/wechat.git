<?php
namespace Sinta\Wechat\OfficialAccount\CustomerService;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 客服务务
 *
 * Class Client
 * @see https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1458044813
 * @package Sinta\Wechat\OfficialAccount\CustomerService
 */
class Client extends BaseClient
{
    /**
     * 获取客服基本信息
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list()
    {
        return $this->httpGet('cgi-bin/customservice/getkflist');
    }

    /**
     * 获取在线客服基本信息
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function online()
    {
        return $this->httpGet('cgi-bin/customservice/getonlinekflist');
    }

    /**
     * 添加客服帐号
     *
     * @param string $account
     * @param string $nickname
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(string $account, string $nickname)
    {
        $params = [
            'kf_account' => $account,
            'nickname' => $nickname,
        ];

        return $this->httpPostJson('customservice/kfaccount/add', $params);
    }

    public function update(string $account, string $nickname)
    {
        $params = [
            'kf_account' => $account,
            'nickname' => $nickname,
        ];

        return $this->httpPostJson('customservice/kfaccount/update', $params);
    }

    /**
     * 删除客服帐号
     *
     * @param string $account
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function delete(string $account)
    {
        return $this->httpPostJson('customservice/kfaccount/del', [], ['kf_account' => $account]);
    }

    /**
     * 邀请绑定客服帐号
     *
     * @param string $account
     * @param string $wechatId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function invite(string $account, string $wechatId)
    {
        $params = [
            'kf_account' => $account,
            'invite_wx' => $wechatId,
        ];

        return $this->httpPostJson('customservice/kfaccount/inviteworker', $params);
    }


    /**
     * 上传客服头像
     *
     * @param string $account
     * @param string $path
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function setAvatar(string $account, string $path)
    {
        return $this->httpUpload('customservice/kfaccount/uploadheadimg', ['media' => $path], [], ['kf_account' => $account]);
    }

    public function message($message)
    {
        $messageBuilder = new Messenger($this);

        return $messageBuilder->message($message);
    }

    public function send(array $message)
    {
        return $this->httpPostJson('cgi-bin/message/custom/send', $message);
    }

    public function messages($startTime, $endTime, int $msgId = 1, int $number = 10000)
    {
        $params = [
            'starttime' => is_numeric($startTime) ? $startTime : strtotime($startTime),
            'endtime' => is_numeric($endTime) ? $endTime : strtotime($endTime),
            'msgid' => $msgId,
            'number' => $number,
        ];

        return $this->httpPostJson('customservice/msgrecord/getmsglist', $params);
    }

}
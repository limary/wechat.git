<?php
namespace Sinta\Wechat\OfficialAccount\CustomerService;

use Pimple\Container;
use Pimple\ServiceProviderInterface;


/**
 * 客服服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount\CustomerService
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['customer_service'] = function ($app) {
            return new Client($app);
        };

        $app['customer_service_session'] = function ($app) {
            return new SessionClient($app);
        };
    }
}
<?php
namespace Sinta\Wechat\OfficialAccount\CustomerService;

use Sinta\Wechat\Kernel\Client as BaseClient;

class SessionClient extends BaseClient
{
    /**
     * 获取客服会话列表
     *
     * @param string $account 完整客服帐号，格式为：帐号前缀@公众号微信号
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list(string $account)
    {
        return $this->httpGet('customservice/kfsession/getsessionlist', ['kf_account' => $account]);
    }

    /**
     * 获取未接入会话列表
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function waiting()
    {
        return $this->httpGet('customservice/kfsession/getwaitcase');
    }

    /**
     * 创建会话
     *
     * @param string $account 完整客服帐号，格式为：帐号前缀@公众号微信号
     * @param string $openid 粉丝的openid
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function create(string $account, string $openid)
    {
        $params = [
            'kf_account' => $account,
            'openid' => $openid,
        ];

        return $this->httpPostJson('customservice/kfsession/create', $params);
    }

    /**
     * 关闭会话
     *
     * @param string $account 完整客服帐号，格式为：帐号前缀@公众号微信号
     * @param string $openid 粉丝的openid
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function close(string $account, string $openid)
    {
        $params = [
            'kf_account' => $account,
            'openid' => $openid,
        ];

        return $this->httpPostJson('customservice/kfsession/close', $params);
    }

    /**
     * 获取客户会话状态
     *
     * 此接口获取一个客户的会话，如果不存在，则kf_account为空。
     * @param string $openid
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(string $openid)
    {
        return $this->httpGet('customservice/kfsession/getsession', ['openid' => $openid]);
    }

}
<?php
namespace Sinta\Wechat\OfficialAccount\Card;


class MeetingTicketClient extends Client
{
    public function updateUser(array $params)
    {
        return $this->httpPostJson('card/meetingticket/updateuser', $params);
    }
}
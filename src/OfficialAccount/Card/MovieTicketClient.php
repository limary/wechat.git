<?php
namespace Sinta\Wechat\OfficialAccount\Card;


class MovieTicketClient extends Client
{

    public function updateUser(array $params)
    {
        return $this->httpPostJson('card/movieticket/updateuser', $params);
    }
}
<?php
namespace Sinta\Wechat\OfficialAccount\Card;

use Sinta\Wechat\Kernel\Client as BaseClient;

class CoinClient extends BaseClient
{
    public function activate()
    {
        return $this->httpGet('card/pay/activate');
    }

    public function getPrice(string $cardId, int $quantity)
    {
        return $this->httpPostJson('card/pay/getpayprice', [
            'card_id' => $cardId,
            'quantity' => $quantity,
        ]);
    }

    public function summary()
    {
        return $this->httpGet('card/pay/getcoinsinfo');
    }

    public function recharge(int $count)
    {
        return $this->httpPostJson('card/pay/recharge', [
            'coin_count' => 100,
        ]);
    }

    public function order(string $orderId)
    {
        return $this->httpPostJson('card/pay/getorder', ['order_id' => $orderId]);
    }


    public function orders(array $filters)
    {
        return $this->httpPostJson('card/pay/getorderlist', $filters);
    }

    public function confirm(string $cardId, string $orderId, int $quantity)
    {
        return $this->httpPostJson('card/pay/confirm', [
            'card_id' => $cardId,
            'order_id' => $orderId,
            'quantity' => $quantity,
        ]);
    }

}
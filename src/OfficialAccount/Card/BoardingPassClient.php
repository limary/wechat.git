<?php
namespace Sinta\Wechat\OfficialAccount\Card;


class BoardingPassClient extends Client
{
    public function checkin(array $params)
    {
        return $this->httpPostJson('card/boardingpass/checkin', $params);
    }
}
<?php
namespace Sinta\Wechat\OfficialAccount\Menu;


use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 公共号自定义菜单服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount
 */
class ServiceProvider implements ServiceProviderInterface
{

    public function register(Container $app)
    {
        $app['menu'] = function ($app){
            return new Client($app);
        };
    }
}
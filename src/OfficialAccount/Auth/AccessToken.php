<?php
namespace Sinta\Wechat\OfficialAccount\Auth;

use Sinta\Wechat\Kernel\AccessToken as BaseAccessToken;

/**
 * 公众号认证TOKEN
 *
 * Class AccessToken
 * @package Sinta\Wechat\OfficialAccount\Auth
 */
class AccessToken extends BaseAccessToken
{
    protected $endpointToGetToken = 'https://api.weixin.qq.com/cgi-bin/token';

    protected function getCredentials(): array
    {
        return [
            'grant_type' => 'client_credential',
            'appid' => $this->app['config']['app_id'],
            'secret' => $this->app['config']['secret'],
        ];
    }
}
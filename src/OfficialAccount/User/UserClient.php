<?php
namespace Sinta\Wechat\OfficialAccount\User;

use Sinta\Wechat\Kernel\Client as BaseClient;

/**
 * 用户
 *
 * Class UserClient
 * @package Sinta\Wechat\OfficialAccount\User
 */
class UserClient extends BaseClient
{
    /**
     *
     * 开发者可通过OpenID来获取用户基本信息。请使用https协议。
     *
     * @param string $openid
     * @param string $lang
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function get(string $openid, string $lang = 'zh_CN')
    {
        $params = [
            'openid' => $openid,
            'lang' => $lang,
        ];
        return $this->httpGet('cgi-bin/user/info', $params);
    }

    /**
     * 批量获取用户信息
     *
     * @param array $openids
     * @param string $lang
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function select(array $openids, string $lang = 'zh_CN')
    {
        return $this->httpPostJson('cgi-bin/user/info/batchget', [
            'user_list' => array_map(function ($openid) use ($lang) {
                return [
                    'openid' => $openid,
                    'lang' => $lang,
                ];
            }, $openids),
        ]);
    }

    /**
     * 获取用户列表

        公众号可通过本接口来获取帐号的关注者列表，关注者列表由一串OpenID（加密后的微信号，每个用户对每个公众号的OpenID是唯一的）组成。
     * 一次拉取调用最多拉取10000个关注者的OpenID，可以通过多次拉取的方式来满足需求
     *
     *
     * @param string|null $nextOpenId  第一个拉取的OPENID
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function list(string $nextOpenId = null)
    {
        $params = ['next_openid' => $nextOpenId];
        return $this->httpGet('cgi-bin/user/get', $params);
    }

    /**
     * 设置用户备注名
     *
     * @param string $openid
     * @param string $remark
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function remark(string $openid, string $remark)
    {
        $params = [
            'openid' => $openid,
            'remark' => $remark,
        ];
        return $this->httpPostJson('cgi-bin/user/info/updateremark', $params);
    }

    /**
     * 获取公众号的黑名单列表
     *
     * @param string|null $beginOpenid
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function blacklist(string $beginOpenid = null)
    {
        $params = ['begin_openid' => $beginOpenid];
        return $this->httpPostJson('cgi-bin/tags/members/getblacklist', $params);
    }

    /**
     * 拉黑用户
     *
     * @param $openidList
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function black($openidList)
    {
        $params = ['openid_list' => (array) $openidList];
        return $this->httpPostJson('cgi-bin/tags/members/batchblacklist', $params);
    }

    /**
     * 取消拉黑用户
     *
     * @param $openidList
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function unblock($openidList)
    {
        $params = ['openid_list' => (array) $openidList];
        return $this->httpPostJson('cgi-bin/tags/members/batchunblacklist', $params);
    }

    public function changeOpenid(string $oldAppId, array $openidList)
    {
        $params = [
            'from_appid' => $oldAppId,
            'openid_list' => $openidList,
        ];
        return $this->httpPostJson('cgi-bin/changeopenid', $params);
    }

}
<?php
namespace Sinta\Wechat\OfficialAccount\Broadcasting;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

/**
 * 厂播服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OfficialAccount
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['broadcasting'] = function ($app) {
            return new Client($app);
        };
    }
}
<?php
namespace Sinta\Wechat\OfficialAccount\Broadcasting;


use Sinta\Wechat\Kernel\Contracts\MessageInterface;
use Sinta\Wechat\Kernel\Exceptions\RuntimeException;


class MessageBuilder
{
    protected $to = [];

    /**
     * @var \Sinta\Wechat\Kernel\Contracts\MessageInterface
     */
    protected $message;

    /**
     * Set message.
     *
     * @param \Sinta\Wechat\Kernel\Contracts\MessageInterface $message
     *
     * @return $this
     */
    public function message(MessageInterface $message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Set target user or group.
     *
     * @param mixed $to
     *
     * @return $this
     */
    public function to(array $to)
    {
        $this->to = $to;

        return $this;
    }

    public function toTag(int $tagId)
    {
        $this->to([
            'filter' => [
                'is_to_all' => false,
                'tag_id' => $tagId,
            ],
        ]);
        return $this;
    }


    public function toUsers(array $openids)
    {
        $this->to([
            'touser' => $openids,
        ]);
        return $this;
    }

    public function toAll()
    {
        $this->to([
            'filter' => ['is_to_all' => true],
        ]);
        return $this;
    }


    /**
     * Build message.
     *
     * @param array $prepends
     *
     * @return array
     *
     * @throws \Sinta\Wechat\Kernel\Exceptions\RuntimeException
     */
    public function build(array $prepends = []): array
    {
        if (empty($this->message)) {
            throw new RuntimeException('No message content to send.');
        }

        $content = $this->message->transformForJsonRequest();

        if (empty($prepends)) {
            $prepends = $this->to;
        }

        $message = array_merge($prepends, $content);

        return $message;
    }


    /**
     * @param string $by
     * @param string $user
     * @return array
     * @throws RuntimeException
     */
    public function buildForPreview(string $by, string $user): array
    {
        return $this->build([$by => $user]);
    }

}
<?php
namespace Sinta\Wechat\OpenPlatform\Auth;

use Sinta\Wechat\Kernel\Exceptions\RuntimeException;
use Sinta\Wechat\Kernel\Traits\InteractsWithCache;
use Sinta\Wechat\OpenPlatform\Application;

/**
 * Class VerifyTicket
 * @package Sinta\Wechat\OpenPlatform\Auth
 */
class VerifyTicket
{
    use InteractsWithCache;

    protected $app;


    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * 设置ticket
     *
     * @param string $ticket
     * @return $this
     * @throws RuntimeException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function setTicket(string $ticket)
    {
        $this->getCache()->set($this->getCacheKey(),$ticket,3600);
        if (!$this->getCache()->has($this->getCacheKey())) {
            throw new RuntimeException('Failed to cache verify ticket.');
        }
        return $this;
    }

    /**
     * 获取ticket
     *
     * @return string
     * @throws RuntimeException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getTicket(): string
    {
        if($cached = $this->getCache()->get($this->getCacheKey())){
            return $cached;
        }
        throw new RuntimeException('Credential "component_verify_ticket" does not exist in cache.');
    }

    /**
     * 获取缓存Key
     *
     * @return string
     */
    protected function getCacheKey(): string
    {
        return 'sinta-wechat.open_platform.component_verify_ticket.'.$this->app['config']['app_id'];
    }
}
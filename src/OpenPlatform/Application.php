<?php
namespace Sinta\Wechat\OpenPlatform;


use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\MiniProgram\Encryptor;
use Sinta\Wechat\OpenPlatform\Auth\AccessToken;
use Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Application as MiniProgram;
use Sinta\Wechat\OpenPlatform\Authorizer\OfficialAccount\Application as OfficialAccount;
use Sinta\Wechat\OpenPlatform\Authorizer\MiniProgram\Auth\Client;
use Sinta\Wechat\OpenPlatform\Authorizer\Server\Guard;
use Sinta\Wechat\OpenPlatform\Authorizer\OfficialAccount\OAuth\ComponentDelegate;

/**
 * 平台应用
 *
 * Class Application
 * @package Sinta\Wechat\OpenPlatform
 */
class Application extends ServiceContainer
{
    /**
     * 开放到台服务
     *
     * @var array
     */
    protected $providers = [
        Auth\ServiceProvider::class,
        Base\ServiceProvider::class,
        Server\ServiceProvider::class,
        CodeTemplate\ServiceProvider::class,
        Component\ServiceProvider::class,
    ];

    /**
     * 默认设置
     *
     * @var array
     */
    protected $defaultConfig = [
        'http' => [
            'timeout' => 5.0,
            'base_uri' => 'https://api.weixin.qq.com/',
        ],
    ];

    /**
     * 获取授权的公众号的应用
     *
     * @param string $appId
     * @param string $refreshToken
     * @param AccessToken|null $accessToken
     * @return OfficialAccount
     */
    public function officialAccount(string $appId,
                                    string $refreshToken,
                                    AccessToken $accessToken = null):OfficialAccount
    {
        $application = new OfficialAccount($this->getAuthorizerConfig($appId, $refreshToken), $this->getReplaceServices($accessToken) + [
                'encryptor' => $this['encryptor'],
                'account' => function ($app) {
                    return new AccountClient($app, $this);
                },
            ]);

        $application->extend('oauth', function ($socialite) {
            return $socialite->component(new ComponentDelegate($this));
        });

        return $application;
    }


    /**
     * 获取授权的小程序
     *
     * @param string $appId
     * @param string $refreshToken
     * @param AccessToken|null $accessToken
     * @return MiniProgram
     */
    public function miniProgram(string $appId,string $refreshToken,
                                AccessToken $accessToken = null):MiniProgram
    {
        return new MiniProgram($this->getAuthorizerConfig($appId,$refreshToken),
            $this->getReplaceServices($accessToken) + [
                'encryptor' => function(){
                    return new Encryptor($this['config']['app_id'], $this['config']['token'], $this['config']['aes_key']);
                },
                'auth' => function($app){
                    return new Client($app,$this);
                }
            ]);
    }


    /**
     * 引入用户进入授权页
     *
     * “微信公众号授权”或者“小程序授权”的入口，引导公众号和小程序管理员进入授权页
     * @param string $callbackUrl
     * @param string|null $authCode
     * @return string
     */
    public function getPreAuthorizationUrl(string $callbackUrl, $optional = []): string
    {
        if (\is_string($optional)) {
            $optional = [
                'pre_auth_code' => $optional,
            ];
        } else {
            $optional['pre_auth_code'] = $this->createPreAuthorizationCode()['pre_auth_code'];
        }

        $queries = \array_merge($optional, [
            'component_appid' => $this['config']['app_id'],
            'redirect_uri' => $callbackUrl,
        ]);
        return 'https://mp.weixin.qq.com/cgi-bin/componentloginpage?'.http_build_query($queries);
    }

    public function getMobilePreAuthorizationUrl(string $callbackUrl, $optional = []): string
    {
        // 兼容旧版 API 设计
        if (\is_string($optional)) {
            $optional = [
                'pre_auth_code' => $optional,
            ];
        } else {
            $optional['pre_auth_code'] = $this->createPreAuthorizationCode()['pre_auth_code'];
        }
        $queries = \array_merge($optional, [
            'component_appid' => $this['config']['app_id'],
            'redirect_uri' => $callbackUrl,
            'action' => 'bindcomponent',
            'no_scan' => 1,
        ]);
        return 'https://mp.weixin.qq.com/safe/bindcomponent?'.http_build_query($queries).'#wechat_redirect';
    }


    protected function getAuthorizerConfig(string $appId, string $refreshToken): array
    {
        return $this['config']->merge([
            'component_app_id' => $this['config']['app_id'],
            'app_id' => $appId,
            'refresh_token' => $refreshToken,
        ])->toArray();
    }


    protected function getReplaceServices(AccessToken $accessToken = null): array
    {
        $services = [
            'access_token' => $accessToken ?? function ($app) {
                    return new AccessToken($app, $this);
                },

            'server' => function ($app) {
                return new Guard($app);
            },
        ];

        foreach(['cache','http_client','log','logger','request'] as $reuse){
            if (isset($this[$reuse])) {
                $services[$reuse] = $this[$reuse];
            }
        }

        return $services;
    }


    /**
     * 动态调用基础服务
     *
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array([$this['base'], $method], $args);
    }

}
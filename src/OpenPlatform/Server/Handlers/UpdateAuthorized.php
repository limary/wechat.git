<?php
namespace Sinta\Wechat\OpenPlatform\Server\Handlers;


use Sinta\Wechat\Kernel\Contracts\EventHandlerInterface;

class UpdateAuthorized implements EventHandlerInterface
{
    public function handle($payload = null)
    {
        // Do nothing for the time being.
    }
}
<?php
namespace Sinta\Wechat\OpenPlatform\Authorizer\OfficialAccount\MiniProgram;




use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{

    /**
     * 获取公众号关联的小程序.
     *
     * @return array|\Sinta\Wechat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     *
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidConfigException
     */
    public function list()
    {
        return $this->httpPostJson('cgi-bin/wxopen/wxamplinkget');
    }
    /**
     * 关联小程序.
     *
     * @param string $appId
     * @param bool   $notifyUsers
     * @param bool   $showProfile
     *
     * @return array|\Sinta\Wechat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     *
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidConfigException
     */
    public function link(string $appId, bool $notifyUsers = true, bool $showProfile = false)
    {
        $params = [
            'appid' => $appId,
            'notify_users' => (string) $notifyUsers,
            'show_profile' => (string) $showProfile,
        ];
        return $this->httpPostJson('cgi-bin/wxopen/wxamplink', $params);
    }
    /**
     * 解除已关联的小程序.
     *
     * @param string $appId
     *
     * @return array|\Sinta\Wechat\Kernel\Support\Collection|object|\Psr\Http\Message\ResponseInterface|string
     *
     * @throws \Sinta\Wechat\Kernel\Exceptions\InvalidConfigException
     */
    public function unlink(string $appId)
    {
        $params = [
            'appid' => $appId,
        ];
        return $this->httpPostJson('cgi-bin/wxopen/wxampunlink', $params);
    }
}
<?php
namespace Sinta\Wechat\OpenPlatform\Base;

use Pimple\Container;
use Pimple\ServiceProviderInterface;


/**
 * 开放平台基础服务
 *
 * Class ServiceProvider
 * @package Sinta\Wechat\OpenPlatform\Base
 */
class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['base'] = function ($app){
            return new Client($app);
        };
    }
}
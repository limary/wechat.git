<?php
namespace Sinta\Wechat\OpenPlatform\Base;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * 获取授权信息
     *
     * @param string|null $authCode
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function handleAuthorize(string $authCode = null)
    {
        $params = [
            'component_appid' => $this->app['config']['app_id'],
            'authorization_code' => $authCode ?? $this->app['request']->get('auth_code'),
        ];

        return $this->httpPostJson('cgi-bin/component/api_query_auth', $params);
    }

    /**
     * 被授权人信息
     *
     * @param string $appId
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getAuthorizer(string $appId)
    {
        $params = [
            'component_appid' => $this->app['config']['app_id'],
            'authorizer_appid' => $appId,
        ];

        return $this->httpPostJson('cgi-bin/component/api_get_authorizer_info', $params);
    }

    public function getAuthorizerOption(string $appId, string $name)
    {
        $params = [
            'component_appid' => $this->app['config']['app_id'],
            'authorizer_appid' => $appId,
            'option_name' => $name,
        ];

        return $this->httpPostJson('cgi-bin/component/api_get_authorizer_option', $params);
    }

    public function setAuthorizerOption(string $appId, string $name, string $value)
    {
        $params = [
            'component_appid' => $this->app['config']['app_id'],
            'authorizer_appid' => $appId,
            'option_name' => $name,
            'option_value' => $value,
        ];

        return $this->httpPostJson('cgi-bin/component/api_set_authorizer_option', $params);
    }


    public function getAuthorizers($offset = 0, $count = 500)
    {
        $params = [
            'component_appid' => $this->app['config']['app_id'],
            'offset' => $offset,
            'count' => $count,
        ];

        return $this->httpPostJson('cgi-bin/component/api_get_authorizer_list', $params);
    }


    /**
     * 获取预授权码pre_auth_code
     *
     * @return array|mixed|null|\Psr\Http\Message\ResponseInterface|static
     */
    public function createPreAuthorizationCode()
    {
        $params = [
            'component_appid' => $this->app['config']['app_id'],
        ];

        return $this->httpPostJson('cgi-bin/component/api_create_preauthcode', $params);
    }

    public function clearQuota()
    {
        $params = [
            'component_appid' => $this->app['config']['app_id'],
        ];
        return $this->httpPostJson('cgi-bin/component/clear_quota', $params);
    }


}
<?php
namespace Sinta\Wechat\OpenPlatform\Component;

use Sinta\Wechat\Kernel\Client as BaseClient;

class Client extends BaseClient
{
    /**
     * 通过法人微信快速创建小程序.
     *
     * @param array $params
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function registerMiniProgram(array $params)
    {
        return $this->httpPostJson('cgi-bin/component/fastregisterweapp', $params, ['action' => 'create']);
    }

    /**
     * 查询创建任务状态.
     *
     * @param string $companyName
     * @param string $legalPersonaWechat
     * @param string $legalPersonaName
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getRegistrationStatus(string $companyName, string $legalPersonaWechat, string $legalPersonaName)
    {
        $params = [
            'name' => $companyName,
            'legal_persona_wechat' => $legalPersonaWechat,
            'legal_persona_name' => $legalPersonaName,
        ];
        return $this->httpPostJson('cgi-bin/component/fastregisterweapp', $params, ['action' => 'search']);
    }
}
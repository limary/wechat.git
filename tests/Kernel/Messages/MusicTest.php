<?php

namespace EasyWeChat\Tests\Kernel\Messages;

use Sinta\Wechat\Kernel\Messages\Music;
use Sinta\Wechat\Tests\TestCase;

class MusicTest extends TestCase
{
    public function testToXmlArray()
    {
        $message = new Music([
                'title' => '告白气球',
                'description' => '告白气球 - 周杰伦',
                'url' => 'http://easywechat.com/music/foo.mp3',
                'hq_url' => 'http://easywechat.com/music/foo_hq.mp3',
                'thumb_media_id' => 'Xhsbdaiu172j321kpsad711x76912ms2klas',
                'format' => 'mp3',
            ]);

        $this->assertSame([
            'Music' => [
                'Title' => '告白气球',
                'Description' => '告白气球 - 周杰伦',
                'MusicUrl' => 'http://easywechat.com/music/foo.mp3',
                'HQMusicUrl' => 'http://easywechat.com/music/foo_hq.mp3',
                'ThumbMediaId' => 'Xhsbdaiu172j321kpsad711x76912ms2klas',
            ],
        ], $message->toXmlArray());
    }
}

<?php
namespace Sinta\Wechat\Tests\OfficialAccount\Auth;

use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\OfficialAccount\Auth\AccessToken;
use Sinta\Wechat\Tests\TestCase;

class AccessTokenTest extends TestCase
{
    public function testGetCredentials()
    {
        $app = new ServiceContainer([
            'app_id' => 'mock-app-id',
            'secret' => 'mock-secret',
        ]);
        $token = \Mockery::mock(AccessToken::class, [$app])->makePartial()->shouldAllowMockingProtectedMethods();

        $this->assertSame([
            'grant_type' => 'client_credential',
            'appid' => 'mock-app-id',
            'secret' => 'mock-secret',
        ], $token->getCredentials());
    }
}

<?php
namespace Sinta\Wechat\Test\OfficialAccount\Card;

use Sinta\Wechat\OfficialAccount\Application;
use Sinta\Wechat\OfficialAccount\Card\BoardingPassClient;
use Sinta\Wechat\OfficialAccount\Card\Card;
use Sinta\Wechat\OfficialAccount\Card\Client;
use Sinta\Wechat\OfficialAccount\Card\CodeClient;
use Sinta\Wechat\OfficialAccount\Card\CoinClient;
use Sinta\Wechat\OfficialAccount\Card\GeneralCardClient;
use Sinta\Wechat\OfficialAccount\Card\JssdkClient;
use Sinta\Wechat\OfficialAccount\Card\MeetingTicketClient;
use Sinta\Wechat\OfficialAccount\Card\MemberCardClient;
use Sinta\Wechat\OfficialAccount\Card\MovieTicketClient;
use Sinta\Wechat\OfficialAccount\Card\SubMerchantClient;
use Sinta\Wechat\Tests\TestCase;

class CardTest extends TestCase
{
    public function testBasicProperties()
    {
        $app = new Application();
        $card = new Card($app);

        $this->assertInstanceOf(Client::class, $card);
        $this->assertInstanceOf(BoardingPassClient::class, $card->boarding_pass);
        $this->assertInstanceOf(MeetingTicketClient::class, $card->meeting_ticket);
        $this->assertInstanceOf(MovieTicketClient::class, $card->movie_ticket);
        $this->assertInstanceOf(CoinClient::class, $card->coin);
        $this->assertInstanceOf(MemberCardClient::class, $card->member_card);
        $this->assertInstanceOf(GeneralCardClient::class, $card->general_card);
        $this->assertInstanceOf(CodeClient::class, $card->code);
        $this->assertInstanceOf(SubMerchantClient::class, $card->sub_merchant);
        $this->assertInstanceOf(JssdkClient::class, $card->jssdk);

        try {
            $card->foo;
            $this->fail('No expected exception thrown.');
        } catch (\Exception $e) {
            $this->assertSame('No card service named "foo".', $e->getMessage());
        }
    }
}

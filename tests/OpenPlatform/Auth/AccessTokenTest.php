<?php


namespace Sinta\Wechat\Tests\OpenPlatform\Auth;

use Sinta\Wechat\Kernel\ServiceContainer;
use Sinta\Wechat\OpenPlatform\Auth\AccessToken;
use Sinta\Wechat\OpenPlatform\Auth\VerifyTicket;
use Sinta\Wechat\Tests\TestCase;

class AccessTokenTest extends TestCase
{
    public function testGetCredentials()
    {
        $verifyTicket = \Mockery::mock(VerifyTicket::class, function ($mock) {
            $mock->expects()->getTicket()->andReturn('ticket@123456')->once();
        });

        $app = new ServiceContainer([
            'app_id' => 'mock-app-id',
            'secret' => 'mock-secret',
        ], ['verify_ticket' => $verifyTicket]);
        $token = \Mockery::mock(AccessToken::class, [$app])->makePartial()->shouldAllowMockingProtectedMethods();

        $this->assertSame([
            'component_appid' => 'mock-app-id',
            'component_appsecret' => 'mock-secret',
            'component_verify_ticket' => 'ticket@123456',
        ], $token->getCredentials());
    }
}

<?php


namespace Sinta\Wechat\Tests\OpenPlatform\Auth;

use Sinta\Wechat\OpenPlatform\Auth\VerifyTicket;
use Sinta\Wechat\Tests\TestCase;
use Psr\SimpleCache\CacheInterface;

class VerifyTicketTest extends TestCase
{
    public function testConstruct()
    {
        $mock = $this->getMockBuilder(VerifyTicket::class)->disableOriginalConstructor()->setMethods(['setTicket'])->getMockForAbstractClass();

        $mock->expects($this->once())->method('setTicket')->with($this->equalTo('ticket@123456'));

        (new \ReflectionClass(VerifyTicket::class))->getConstructor()->invoke($mock, 'app-id', 'ticket@123456');
    }

    public function testSetTicket()
    {
        $client = \Mockery::mock(VerifyTicket::class.'[getCache]', ['app-id'], function ($mock) {
            $cache = \Mockery::mock(CacheInterface::class, function ($mock) {
                $mock->expects()->set('sinta-wechat.open_platform.component_verify_ticket.app-id', 'ticket@654321')->once();
            });
            $mock->allows()->getCache()->andReturn($cache);
        });

        $this->assertInstanceOf(VerifyTicket::class, $client->setTicket('ticket@654321'));
    }

    public function testGetTicket()
    {
        $client = \Mockery::mock(VerifyTicket::class.'[getCache]', ['app-id'], function ($mock) {
            $cache = \Mockery::mock(CacheInterface::class, function ($mock) {
                $mock->expects()->get('sinta-wechat.open_platform.component_verify_ticket.app-id')->andReturn('ticket@123456')->once();
            });
            $mock->allows()->getCache()->andReturn($cache);
        });

        $this->assertSame('ticket@123456', $client->getTicket());
    }

    public function testGetTicketAndThrowException()
    {
        $client = \Mockery::mock(VerifyTicket::class.'[getCache]', ['app-id'], function ($mock) {
            $cache = \Mockery::mock(CacheInterface::class, function ($mock) {
                $mock->expects()->get('sinta-wechat.open_platform.component_verify_ticket.app-id')->andReturn(null)->once();
            });
            $mock->allows()->getCache()->andReturn($cache);
        });

        $this->expectException('Sinta\Wechat\Kernel\Exceptions\RuntimeException');
        $this->expectExceptionMessage('Credential "component_verify_ticket" does not exist in cache.');
        $client->getTicket();
    }
}

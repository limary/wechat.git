<?php


namespace Sinta\Wechat\Tests\OpenPlatform;

use Sinta\Wechat\OpenPlatform\Application;
use Sinta\Wechat\OpenPlatform\Auth\VerifyTicket;
use Sinta\Wechat\OpenPlatform\Server\Handlers\VerifyTicketRefreshed;
use Sinta\Wechat\Tests\TestCase;

class VerifyTicketRefreshedTest extends TestCase
{
    public function testHandle()
    {
        $app = new Application();
        $app['verify_ticket'] = \Mockery::mock(VerifyTicket::class, function ($mock) {
            $mock->expects()->setTicket('ticket')->once();
        });
        $handler = new VerifyTicketRefreshed($app);

        $this->assertNull($handler->handle([
            'ComponentVerifyTicket' => 'ticket',
        ]));
    }
}

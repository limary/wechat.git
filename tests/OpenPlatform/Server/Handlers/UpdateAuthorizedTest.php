<?php

namespace Sinta\Wechat\Tests\OpenPlatform;

use Sinta\Wechat\OpenPlatform\Server\Handlers\UpdateAuthorized;
use Sinta\Wechat\Tests\TestCase;

class UpdateAuthorizedTest extends TestCase
{
    public function testHandle()
    {
        $handler = new UpdateAuthorized();

        $this->assertNull($handler->handle());
    }
}

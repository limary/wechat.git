<?php

namespace Sinta\Wechat\Tests\OpenPlatform\Server;

use Sinta\Wechat\OpenPlatform\Application;
use Sinta\Wechat\OpenPlatform\Server\Guard;
use Sinta\Wechat\Tests\TestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GuardTest extends TestCase
{
    public function testResolve()
    {
        $request = Request::create('/path/to/resource', 'POST', [], [], [], [
        'CONTENT_TYPE' => ['application/xml'],
    ], '<xml><AppId>wx-appid</AppId><InfoType>component_verify_ticket</InfoType></xml>');

        $app = new Application([], [
            'request' => $request,
        ]);
        $guard = \Mockery::mock(Guard::class, [$app])->makePartial();

        $this->assertInstanceOf(Response::class, $guard->resolve());
        $this->assertSame('success', $guard->resolve()->getContent());
    }
}
